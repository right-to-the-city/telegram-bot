# Right To The City Telegram Bot

Телеграм бот проекта Право на Город

## Установка

Создать виртуальное окружение по желанию

```sh
pip install -r requirements.txt
```

Создать файл `config.yml` со следующим содержимым:

```yml
bot_token: <Токен телеграм бота>
api_root: <Ссылка на сервер с API>
locale: <en|ru - язык бота>
```

## Запуск

```sh
python -m bot
```