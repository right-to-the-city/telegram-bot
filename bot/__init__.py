__all__ = ['bot', 'dp', 'client', 'config', 'categories', 'Category', 'strings']

import logging
import json

from typing import Optional

from aiohttp.client import ClientSession

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.files import JSONStorage

from common.config import Config

from .strings import Strings

logging.basicConfig(level=logging.INFO)

# Загрузка конфигурации
config_path = 'config.yml'
config = Config.from_yaml(config_path)
logging.getLogger().info(f'Reading config from {config_path}')

# Сессия для обращения к серверу
client = ClientSession(config.api_root)

API_TOKEN = config.bot_token

# Инициализация бота и диспетчера
bot = Bot(token=API_TOKEN)
storage = JSONStorage('dynamic/user_data.json')
dp = Dispatcher(bot, storage=storage)

# TODO запрашивать категории у сервера
# Чтение категорий из файла 
Category = dict[str, Optional['Category']]

with open('categories.json', 'r', encoding='utf-8') as f:
    categories: Category = json.load(f)

strings = Strings.from_yaml(f'bot/strings/{config.locale}.yml')

# Импортируем команды
from . import commands
