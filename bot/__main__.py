import logging
import sys
from argparse import ArgumentParser

from aiogram import executor
from aiohttp import ClientSession

from . import dp


executor.start_polling(dp, skip_updates=True)
