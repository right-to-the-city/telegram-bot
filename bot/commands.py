import os.path
import re
from tempfile import TemporaryDirectory
from traceback import print_exc

from aiogram.types import Message, CallbackQuery, InputMediaPhoto
from datetime import datetime
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiohttp import FormData

from . import bot, dp, categories, client, strings, config
from .keyboards import Keyboards


def get_category(keys: tuple[str, ...]):
    cat = categories

    while keys:
        key, *keys = keys
        cat = cat[key]

    return cat


class Survey(StatesGroup):
    category = State()  # Выбор категории
    media = State()     # Фото или видео
    rating = State()    # Оценка
    comment = State()   # Комментарий
    tags = State()      # Хештеги
    location = State()  # Местоположение на геолокации
    submit = State()   # Подтвердить отправку


class Pagination(StatesGroup):
    offset = State()

class HandlerGroup:
    states_group: type[StatesGroup]

    @classmethod
    def on(cls, state: str | State | None):
        if state is None:
            state = 'none'
        elif isinstance(state, State):
            state = state.state

        if ':' in state:
            state = state.replace(':', '_')

        state = state.lower()

        return getattr(cls, f'on_{state}', None)
    
    @classmethod
    async def next_state(cls, message: Message, context: FSMContext = None):
        await cls.states_group.next()

        return await cls._call_handler(message, context)
    
    @classmethod
    async def previous_state(cls, message: Message, context: FSMContext = None):
        await cls.states_group.previous()

        return await cls._call_handler(message, context)
    
    @classmethod
    async def set_state(cls, new_state: State, message: Message, context: FSMContext = None):
        await new_state.set()

        return await cls._call_handler(message, context)
    
    @classmethod
    async def finish(cls, message: Message, context: FSMContext = None):
        await context.finish()

        return await cls._call_handler(message, context)
        
    @classmethod
    async def retry(cls, message: Message, context: FSMContext = None):
        return await cls._call_handler(message, context)
        
    @classmethod
    async def _call_handler(cls,  message: Message, context: FSMContext = None):
        state = await context.get_state()
        handler = cls.on(state)
        
        if handler:
            return await handler(message, context)


class Handlers(HandlerGroup):
    states_group = Survey

    @classmethod
    async def on_start(cls, message: Message, state: FSMContext = None):
        return await message.answer(strings.hello, reply_markup=Keyboards.initial)
    
    @classmethod
    async def on_survey_category(cls, message: Message, state: FSMContext = None):
        async with state.proxy() as proxy:
            proxy['category'] = ()

        keyboard = Keyboards.select_category(categories)

        return await message.answer(strings.category.enter, reply_markup=keyboard)
    
    @classmethod
    async def on_survey_media(cls, message: Message, state: FSMContext = None):
        async with state.proxy() as proxy:
            proxy['media'] = {
                'items': [],
                'type': None,
            }

        return await message.answer(strings.media.enter, reply_markup=Keyboards.ready)

    @classmethod
    async def on_survey_rating(cls, message: Message, state: FSMContext = None):
        return await message.answer(
            strings.rating.enter, 
            reply_markup=Keyboards.rating
        )

    @classmethod
    async def on_survey_comment(cls, message: Message, state: FSMContext = None):
        return await message.answer(strings.comment.enter)

    @classmethod
    async def on_survey_tags(cls, message: Message, state: FSMContext = None):
        return await message.answer(strings.tags.enter)

    @classmethod
    async def on_survey_location(cls, message: Message, state: FSMContext = None):
        return await message.answer(
            strings.location.enter, 
            reply_markup=Keyboards.share_location
        )

    @classmethod
    async def on_survey_submit(cls, message: Message, state: FSMContext = None):
        async with state.proxy() as proxy:
            data = {
                **proxy,
                **proxy['location'],
                'category': proxy['category'][-1],
                'media_count': len(proxy['media']['items']),
                'media_type': proxy['media']['type'],
                'rating': strings.rating.emoji[proxy['rating']],
                'tags': ' '.join(proxy['tags'])
            }

        return await message.answer(
            strings.submit.enter.format(**data),
            reply_markup=Keyboards.submit
        )
    
    @classmethod
    async def on_none(cls, message: Message, state: FSMContext = None):
        return await message.answer(strings.what_do_you_want, reply_markup=Keyboards.initial)


# === Команды ===
class Commands:
    @dp.message_handler(commands='start')
    @staticmethod
    async def start(message: Message):
        await Handlers.on_start(message)

    @dp.message_handler(commands=['current', 'state'], state='*')
    @staticmethod
    async def current_state(message: Message, state: FSMContext):
        s = await state.get_state()
        await message.answer(f'{s}')

    @dp.message_handler(commands='set', state='*')
    @staticmethod
    async def set_state(message: Message, state: FSMContext):
        state_name, *_ = message.get_args().split()
        the_state = getattr(Survey, state_name, None)

        if the_state is None or not isinstance(the_state, State):
            await message.answer(f'No state named {state_name!r} exists')
            return

        await Handlers.set_state(the_state, message, state)

    @dp.message_handler(commands='next', state='*')
    @staticmethod
    async def next_state(message: Message, state: FSMContext):
        await Handlers.next_state(message, state)

    @dp.message_handler(commands=['back', 'previous'], state='*')
    @staticmethod
    async def previous_state(message: Message, state: FSMContext):
        await Handlers.previous_state(message, state)

    @dp.message_handler(commands=['reset', 'cancel'], state='*')
    @staticmethod
    async def reset_state(message: Message, state: FSMContext):
        await Handlers.finish(message, state)

    @dp.message_handler(commands='retry', state='*')
    @staticmethod
    async def retry(message: Message, state: FSMContext):
        await Handlers.retry(message, state)

    @dp.message_handler(commands='proxy', state='*')
    @staticmethod
    async def view_proxy(message: Message, state: FSMContext):
        async with state.proxy() as proxy:
            await message.answer(f'Your proxy is {proxy}')

    @dp.message_handler(text=strings.back, state='*')
    @staticmethod
    async def go_back(message: Message, state: FSMContext):
        if await state.get_state() is None:
            return await message.answer(strings.no_way_back)
        
        await Handlers.previous_state(message, state)

    @dp.message_handler(text=strings.cancel, state='*')
    @staticmethod
    async def cancel(message: Message, state: FSMContext):
        await Handlers.finish(message, state)

    @dp.message_handler(commands='crash', state='*')
    @staticmethod
    async def crash(message: Message, state: FSMContext):
        raise Exception('Craaaaaaaash!')
    
    @dp.message_handler(commands='check_user', state='*')
    @staticmethod
    async def check_user(message: Message, state: FSMContext):
        async with client.get(f'/api/user/{message.from_user.id}') as response:
            data = await response.json()

        await message.answer(f'Your data is: {data}')

    @dp.message_handler(commands='username', state='*')
    @staticmethod
    async def username(message: Message, state: FSMContext):
        await message.answer(f'You are {make_username(message.from_user)}')


# === Начало работы ===

@dp.message_handler(text=strings.button_create)
async def handle_create(message: Message, state: FSMContext):
    await message.answer(strings.lets_begin, reply_markup=Keyboards.back_cancel)
    await Handlers.next_state(message, state)


@dp.message_handler(text=strings.button_info)
async def handle_info(message: Message, state: FSMContext):
    await message.answer(
        strings.info.format(button_create=strings.button_create)
    )

@dp.message_handler(text=strings.button_show_objects)
async def handle_show_objects(message: Message, state: FSMContext):
    # Начинаем с offset = 0
    await state.update_data(offset=0)
    await show_surveys(message, state)

@dp.message_handler(text=strings.load_more)
async def handle_load_more(message: Message, state: FSMContext):
    await show_surveys(message, state)

async def show_surveys(message: Message, state: FSMContext):
    loading_message = await message.answer("Загружается...", reply_markup=Keyboards.load_more_cancel())
    
    data = await state.get_data()
    offset = data.get('offset', 0)
    limit = 5

    try:
        async with client.get(f'/api/map?offset={offset}&limit={limit}') as response:
            if response.status == 200:
                result = await response.json()
                surveys = result['data']
                total = result['total']

                for item in surveys:
                    date = datetime.strptime(item['date'], '%Y-%m-%d').strftime('%d.%m.%Y')

                    text_message = (
                        f"👤 Пользователь: {item['user']['username']}\n"
                        f"📂 Категория: {item['category']}\n"
                        f"💬 Комментарий: {item['comment']}\n"
                    )

                    if item['tags']:
                        text_message += f"🏷️ Теги: {', '.join(item['tags'])}\n"

                    text_message += f"📅 Дата: {date}\n"

                    if item['media']:
                        media_group = [
                            InputMediaPhoto(media=config.api_root + '/api' + media_url) 
                            for media_url in item['media']
                        ]
                        if len(media_group) > 0:
                            media_group[0].caption = text_message
                            await message.answer_media_group(media_group)
                        else:
                            await message.answer(text_message)
                        
                    else:
                        await message.answer(text_message)

                new_offset = offset + limit
                await state.update_data(offset=new_offset)

                if new_offset < total:
                    await message.answer("Выберите действие:", reply_markup=Keyboards.load_more_cancel())
                else:
                    await message.answer("Все элементы загружены.", reply_markup=Keyboards.cancel())
            else:
                await loading_message.edit_text("Ошибка при загрузке данных.")
    except Exception as e:
        await loading_message.edit_text(f"Произошла ошибка: {str(e)}")

# === Обработчики для состояний ===

# Категория
@dp.callback_query_handler(state=Survey.category)
async def handle_category(callback: CallbackQuery, state: FSMContext):
    async with state.proxy() as proxy:
        current = proxy['category']

        # TODO check for valid category
        if callback.data == 'back':
            current = current[:-1]
        else:
            current += (callback.data,)

        proxy['category'] = current

    # TODO try except
    category = get_category(current)

    if category is None:
        # Клавиатура удаляется автоматом
        await callback.message.edit_text(
            strings.category.selected.format(callback.data)
        )

        # Не знаю как, но callback.message работает как надо...
        await Handlers.next_state(callback.message, state)

    else:
        keyboard = Keyboards.select_category(category, back=len(current) > 0)
        await callback.message.edit_reply_markup(keyboard)
    
    await callback.answer()


# Медиа
@dp.message_handler(text=strings.ready, state=Survey.media)
async def handle_media_submit(message: Message, state: FSMContext):
    async with state.proxy() as proxy:
        if not proxy['media']['items']:
            return await message.answer(strings.media.empty)

    await message.answer(strings.got_it, reply_markup=Keyboards.back_cancel)
    await Handlers.next_state(message, state)


@dp.message_handler(content_types=['photo', 'video'], state=Survey.media)
async def handle_media(message: Message, state: FSMContext):
    async with state.proxy() as proxy:
        if message.content_type == 'photo':
            if proxy['media']['type'] == 'video':
                return await message.answer(strings.media.cant_mix_and_match)
                
            if len(proxy['media']['items']) >= 10:
                return await message.answer(strings.media.too_many_photos)
            
            photo = message.photo.pop()
            proxy['media']['items'].append(photo.file_id)
            proxy['media']['type'] = 'photo'
        
        else:
            if proxy['media']['type'] == 'photo':
                return await message.answer(strings.media.cant_mix_and_match)
            
            if len(proxy['media']['items']) >= 1:
                return await message.answer(strings.media.too_many_videos)

            if message.video.duration > 10:
                return await message.answer(strings.media.video_too_long)
            
            proxy['media']['items'].append(message.video.file_id)
            proxy['media']['type'] = 'video'


@dp.message_handler(content_types=['document'], state=Survey.media)
async def handle_media_document(message: Message, state: FSMContext):
    await message.reply(strings.media.document)


@dp.message_handler(content_types=['any'], state=Survey.media)
async def handle_media_wrong_type(message: Message, state: FSMContext):
    await message.reply(strings.media.wrong_type)


# Рейтинг
@dp.callback_query_handler(state=Survey.rating)
async def handle_rating(callback: CallbackQuery, state: FSMContext):
    if callback.data not in '012345678910':
        return await callback.answer(strings.wrong_button_kronk, True)

    async with state.proxy() as proxy:
        rating = int(callback.data)
        proxy['rating'] = rating

        await callback.message.edit_text(
            strings.rating.selected.format(strings.rating.emoji[rating])
        )
    
    await Handlers.next_state(callback.message, state)


# Комментарий
@dp.message_handler(content_types=['text'], state=Survey.comment)
async def handle_comment(message: Message, state: FSMContext):
    if not message.text:
        return await message.reply(strings.comment.wrong_type)
    
    async with state.proxy() as proxy:
        proxy['comment'] = message.text

    await Handlers.next_state(message, state)


@dp.message_handler(content_types=['any'], state=Survey.comment)
async def handle_comment_wrong_type(message: Message, state: FSMContext):
    await message.reply(strings.comment.wrong_type)

# Теги
TAG = r'\B((?:#|＃)\w+\b)'

@dp.message_handler(content_types=['text'], state=Survey.tags)
async def handle_tags(message: Message, state: FSMContext):
    tags = re.findall(TAG, message.text)

    if not tags:
        return await message.reply(strings.tags.empty)

    async with state.proxy() as proxy:
        proxy['tags'] = tags

    await Handlers.next_state(message, state)


@dp.message_handler(content_types=['any'], state=Survey.tags)
async def handle_not_tags(message: Message, state: FSMContext):
    await message.reply(strings.tags.wrong_type)

# Геолокация

@dp.message_handler(content_types=['location'], state=Survey.location)
async def handle_location(message: Message, state: FSMContext):
    async with state.proxy() as proxy:
        proxy['location'] = {
            'lat': message.location.latitude,
            'lon': message.location.longitude,
        }

    await Handlers.next_state(message, state)


@dp.message_handler(text=strings.location.button, state=Survey.location)
async def handle_location_not_supported(message: Message, state: FSMContext):
    await message.reply(strings.location.device_not_supported)


@dp.message_handler(content_types=['any'], state=Survey.location)
async def handle_not_location(message: Message, state: FSMContext):
    await message.reply(strings.location.wrong_type)


@dp.message_handler(text=strings.submit.button, state=Survey.submit)
async def submit_survey(message: Message, state: FSMContext):
    async with state.proxy() as proxy:
        try:
            await ensure_user(message.from_user)
        except Exception:
            await message.answer(strings.errors.user_error.format(tg_chat=config.tg_chat))
            print_exc()
            return

        try:
            async with client.post('/api/survey', json={
                'user_id': message.from_id,
                'category': proxy['category'][-1],
                'comment': proxy['comment'],
                'rating': proxy['rating'],
                # TODO отправлять списком
                'tags': ' '.join(proxy['tags']),
                'position': proxy['location']
            }) as response:
                # TODO check for errors
                response_data = await response.json()
                survey_id = response_data['id']
        except Exception:
            await message.answer(strings.errors.submit_error.format(tg_chat=config.tg_chat))
            print_exc()
            return

        try:
            await upload_media(proxy['media']['items'], survey_id)
        except Exception:
            await message.answer(strings.errors.media_upload_error.format(tg_chat=config.tg_chat))
            print_exc()
            return
    
    await message.answer(strings.submit.submitted)
    await Handlers.finish(message, state)

# === Обработка невалидного ввода ===

# Обработка устарвших коллбэков
@dp.callback_query_handler(state='*')
async def handle_rogue_callback(callback: CallbackQuery, state: FSMContext):
    await callback.answer(strings.too_late, True)

# Обработка невалидных входных данных
@dp.message_handler(state='*')
async def catch_all(message: Message, state):
    await message.answer(strings.bad_command)

# === Обработка ошибок ===

@dp.errors_handler()
async def handle_errors(update, exception):
    await update.message.answer(
        strings.errors.generic.format(tg_chat=config.tg_chat, cancel=strings.cancel)
    )


# === Вспомогательные функции ===

# Собрать имя пользователя из юзернейма имени и фамилии
def make_username(user):
    username = user.first_name

    if user.last_name:
        username += f' {user.last_name}'

    if user.username:
        username += f' ({user.username})'

    return username

# Создать пользователя, если такой не существует
async def ensure_user(user):
    async with client.head(f'/api/user/{user.id}') as response:
        if response.status == 200:
            return
        
        elif response.status != 404:
            data = await response.json()
            raise Exception(f'Error fetching user: {data}')
    
    async with await client.post('/api/user', json={
        'id': user.id,
        'username': make_username(user)
    }) as response:
        
        if response.status != 201:
            data = await response.json()
            raise Exception(f'Error when ensuring user: {data}')




# Загрузить медиафайлы 
async def upload_media(media: list[int], survey_id: int):
    with TemporaryDirectory() as tmpdir:
        paths = []

        for file_id in media:
            file = await bot.get_file(file_id)
            _, filename = os.path.split(file.file_path)

            filepath = os.path.join(tmpdir, filename)
            paths.append(
                (filename, filepath)
            )

            await file.download(destination_file=filepath)

        form_data = FormData()

        for name, path in paths:
            form_data.add_field(name, open(path, 'rb'), filename=name)

        await client.post(f'/api/survey/{survey_id}/media', data=form_data)