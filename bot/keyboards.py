from typing import Any

from aiogram.types import KeyboardButton, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup

from . import Category, strings

class Keyboards:
    __buttons = [
            InlineKeyboardButton(text, callback_data=str(value)) 
            for text, value 
            in zip(strings.rating.emoji, range(11))
        ]

    rating = InlineKeyboardMarkup(
        inline_keyboard=[
            __buttons[:5],
            __buttons[5:6],
            __buttons[6:]
        ]
    )

    initial = (
        ReplyKeyboardMarkup(resize_keyboard=True)
        .add(KeyboardButton(strings.button_create))
        .add(KeyboardButton(strings.button_show_objects))
        .add(KeyboardButton(strings.button_info))
    )

    ready = (
        ReplyKeyboardMarkup(resize_keyboard=True)
        .add(KeyboardButton(strings.ready))
        .add(KeyboardButton(strings.back))
        .add(KeyboardButton(strings.cancel))
    )

    submit = (
        ReplyKeyboardMarkup(resize_keyboard=True)
        .add(KeyboardButton(strings.submit.button))
        .add(KeyboardButton(strings.back))
        .add(KeyboardButton(strings.cancel))
    )

    back_cancel = (
        ReplyKeyboardMarkup(resize_keyboard=True)
        .add(KeyboardButton(strings.back))
        .add(KeyboardButton(strings.cancel))
    )

    share_location = (
        ReplyKeyboardMarkup(resize_keyboard=True)
        .add(KeyboardButton(strings.location.button, request_location=True))
        .add(KeyboardButton(strings.back))
        .add(KeyboardButton(strings.cancel))
    )

    @staticmethod
    def select_category(category: Category, back: bool = False):
        buttons = [
            InlineKeyboardButton(
                text=key + ' >' if value else key, 
                callback_data=key
            ) 
            for key, value in category.items()
        ]

        if back:
            buttons.append(
                InlineKeyboardButton(strings.category.back, callback_data='back')
            )

        return InlineKeyboardMarkup(row_width=1).add(*buttons)
    
    @staticmethod
    def load_more_cancel():
        return (
            ReplyKeyboardMarkup(resize_keyboard=True)
            .add(KeyboardButton(strings.load_more))
            .add(KeyboardButton(strings.cancel))
        )
    
    @staticmethod
    def cancel():
        return (
            ReplyKeyboardMarkup(resize_keyboard=True)
            .add(KeyboardButton(strings.cancel))
        )
