__all__ = ['Strings']

from dataclasses import dataclass
from common.config import BaseConfig


@dataclass
class Category(BaseConfig):
    enter: str
    selected: str
    back: str


@dataclass
class Media(BaseConfig):
    enter: str

    wrong_type: str
    too_many_photos: str
    too_many_videos: str
    cant_mix_and_match: str
    video_too_long: str
    empty: str
    document: str

@dataclass
class Rating(BaseConfig):
    enter: str
    selected: str
    emoji: list[str]


@dataclass
class Comment(BaseConfig):
    enter: str
    wrong_type: str


@dataclass
class Tags(BaseConfig):
    enter: str
    skip: str
    empty: str
    wrong_type: str


@dataclass
class Location(BaseConfig):
    enter: str
    button: str
    wrong_type: str
    device_not_supported: str


@dataclass
class Submit(BaseConfig):
    enter: str
    button: str
    submitted: str


@dataclass
class Errors(BaseConfig):
    generic: str
    user_error: str
    submit_error: str
    media_upload_error: str


@dataclass
class Strings(BaseConfig):
    hello: str
    what_do_you_want: str

    button_create: str
    button_show_objects: str
    button_info: str
    load_more: str

    lets_begin: str
    info: str

    ready: str
    back: str
    cancel: str

    bad_command: str
    too_late: str
    no_way_back: str
    nothing_to_cancel: str
    wrong_button_kronk: str
    got_it: str

    category: Category
    media: Media
    rating: Rating
    comment: Comment
    tags: Tags
    location: Location
    submit: Submit
    errors: Errors
